﻿# <a id="changelog-heading-root"></a> Change Log

## <a id="changelog-heading-100"></a> 1.0.0

### <a id="changelog-heading-100-features"></a> New Features

- [Example of a changelog entry for a new feature](#changelog-heading-871a8b9)
- [A changelog entry demostrating the linking to Issues, Merge Requests and Milestones](#changelog-heading-b4939b4)

### <a id="changelog-heading-100-bugfixes"></a> Bug Fixes

- [Example of an bugifx changelog entry](#changelog-heading-36afdb6)

### <a id="changelog-heading-100-details"></a> Details

#### <a id="changelog-heading-871a8b9"></a> Example of a changelog entry for a new feature

The description of the feature should be provided here.

Multiple paragraphs (seaprated by blank lines can be provided).

- Commit: [`871a8b9`](https://gitlab.com/ap0llo/changelog-demo/-/commit/871a8b9274a84a688a17ee3a326b28d3ef4b1100)

#### <a id="changelog-heading-b4939b4"></a> A changelog entry demostrating the linking to Issues, Merge Requests and Milestones

In addition to the summary and body, commit messages can contain  
"footers". The typical use case for footers is providing additional  
information about the change, e.g. links to related issues  
or merge requests.

Footers use the format "type: value". Multiple footers can be provided.  
In the case of GitLab, the value can be a reference to an issue, merge  
request or milestone.  
items can be linked using the GitLab refernce syntax (\# for issues,  
\! for merge requests, % for milestones). All references can optionally  
contain a namespace and project name to reference to other projects.  
The footers are separated from the message body using a blank line.

- Issue: [\#1](https://gitlab.com/ap0llo/changelog-demo/-/issues/1)
- Issue: [changelog\-demo\#1](https://gitlab.com/ap0llo/changelog-demo/-/issues/1)
- Issue: [ap0llo\/changelog\-demo\#1](https://gitlab.com/ap0llo/changelog-demo/-/issues/1)
- Merge\-Request: [\!1](https://gitlab.com/ap0llo/changelog-demo/-/merge_requests/1)
- Merge\-Request: [changelog\-demo\!1](https://gitlab.com/ap0llo/changelog-demo/-/merge_requests/1)
- Merge\-Request: [ap0llo\/changelog\-demo\!1](https://gitlab.com/ap0llo/changelog-demo/-/merge_requests/1)
- Milestone: [%1](https://gitlab.com/ap0llo/changelog-demo/-/milestones/1)
- Milestone: [changelog\-demo%1](https://gitlab.com/ap0llo/changelog-demo/-/milestones/1)
- Milestone: [ap0llo\/changelog\-demo%1](https://gitlab.com/ap0llo/changelog-demo/-/milestones/1)
- Commit: [`b4939b4`](https://gitlab.com/ap0llo/changelog-demo/-/commit/b4939b408c414e33890278fbabed1ec2537108d0)

#### <a id="changelog-heading-36afdb6"></a> Example of an bugifx changelog entry

In the body, additional details for the changelog entry can be  
specified.

- Commit: [`36afdb6`](https://gitlab.com/ap0llo/changelog-demo/-/commit/36afdb6e422615f97b93ed88d454b300c99206f2)
